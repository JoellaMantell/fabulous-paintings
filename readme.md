# Paintings

The website provides you some of the best creations of the world. There is a huge collection of mixed media and oil canvas art. Please have a look [here.](http://www.blclawcenter.com/) 

Moreover, some artwork is also available created by me and my sisters. I am learning from an academy since the pats six years. I like to paint oceans, depths, lagoons and mostly use shades of  blues, greens, purples and pinks. I am also trying to gather some fabulous paintings which are still unapproachable to the masses. 